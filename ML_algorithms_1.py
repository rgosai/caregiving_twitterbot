import pandas as pd
import numpy as np
import re
import time
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.preprocessing import LabelEncoder
from collections import defaultdict
from nltk.corpus import wordnet as wn
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import model_selection, naive_bayes, svm
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
import datetime as DT

np.random.seed(500)

Train_clean_tweet = pd.read_excel("C:/Users/rgosai/PycharmProjects/twitter bot/train_clean_labeled_data.xlsx",encoding='utf-8')
Train_filtered_tweet = pd.read_excel("C:/Users/rgosai/PycharmProjects/twitter bot/train_filtered_data.xlsx",encoding='utf-8')
Test_clean_tweet = pd.read_excel("C:/Users/rgosai/PycharmProjects/twitter bot/test_clean_data.xlsx",encoding='utf-8')

Train_clean_tweet = Train_clean_tweet.dropna(subset=["Label"])


Train_clean_tweet['Merged_data'] = "@" + Train_clean_tweet['SenderScreenName'].str.cat(Train_clean_tweet['Message'],sep=" ")

Test_clean_tweet['Merged_data'] = "@" + Test_clean_tweet['SenderScreenName'].str.cat(Test_clean_tweet['Message'],sep=" ")

#print(Train_clean_tweet.info())
#print(Corpus["Merged_data"].head())

time = pd.to_datetime('now')
time = time.replace(microsecond=0)
clean_time = re.sub("-",":", str(time))
weeks_ago = time - DT.timedelta(days=13)
weeks_ago = weeks_ago.replace(microsecond=0)
weeks_ago = re.sub("-",":", str(weeks_ago))

# Train_X = Corpus.loc[Corpus["CreatedTime"] < weeks_ago, "Merged_data"]
# print(Train_X)

TrainX = Train_clean_tweet.drop("Label", axis=1)
print(TrainX.shape, Test_clean_tweet.shape)
test_clean_tweet = Test_clean_tweet.iloc[:,2:]
Data = pd.concat([TrainX, test_clean_tweet], sort=False)
TrainY = Train_clean_tweet["Label"]
Train_Y = TrainY[:470]
#Test_X = Test_clean_tweet.copy()
Data = Data.drop(["Sender_Followers_Count","Merged_data"], axis=1)
Train_X = Data.iloc[0:470,:]
Test_X = Data.iloc[471:,:]

print(Train_X.info(), Test_X.info())
print(Test_X)
#
# # # Train_X, Test_X, Train_Y, Test_Y = model_selection.train_test_split(Train_clean_tweet['Merged_data'],Train_clean_tweet['Label'],test_size=0.3)

#
Encoder = LabelEncoder()
Train_Y = Encoder.fit_transform(Train_Y)
# print(Train_Y)
# Test_Y = Encoder.fit_transform(Test_Y)

# #
# # print(Train_Y)
# # print("-----------------------------------")
# # print(Test_Y)
# # print("-----------------------------------")
#
Tfidf = TfidfVectorizer(analyzer='word', stop_words='english')
#Tfidf_vect = Tfidf.fit(TrainX['Message'])
Train_X_Tfidf = Tfidf.fit_transform(Train_X["Message"])
Test_X_Tfidf = Tfidf.transform(Test_X["Message"])

# Classifier - Algorithm - Naive Bayes
Naive = naive_bayes.MultinomialNB()
Naive.fit(Train_X_Tfidf,Train_Y)
# predict the labels on validation dataset
predictions_NB = Naive.predict(Test_X_Tfidf)

# Classifier - Algorithm - SVM
# fit the training dataset on the classifier
SVM = svm.SVC(C=1.0, kernel='linear', degree=3, gamma='auto')
SVM.fit(Train_X_Tfidf,Train_Y)
# predict the labels on validation dataset
predictions_SVM = SVM.predict(Test_X_Tfidf)

# Classifier - Algorithm - Logistic regression
# fit the training dataset on the classifier
classifier = LogisticRegression()
classifier.fit(Train_X_Tfidf,Train_Y)
# predict the labels on validation dataset
predictions_clf = classifier.predict(Test_X_Tfidf)

#
# Classifier - Algorithm - Random Forests
# fit the training dataset on the classifier
classifier1 = RandomForestClassifier()
classifier1.fit(Train_X_Tfidf,Train_Y)
# predict the labels on validation dataset
predictions_rf = classifier1.predict(Test_X_Tfidf)

pd.set_option('display.max_columns', None)

#using the Naive Bayes predictions and converting it to a Dataframe
data = pd.DataFrame({"Label":predictions_NB})
#resettign the index for merging the data
Test_X = Test_X.reset_index()

Train_filtered_data = train_filtered_data[["UniversalMessageId", "Message"]]



#Merging the test and predictions dataframe
final_data = pd.merge(Test_X, data, left_index=True, right_index=True)
#extracting the individual caregivers
final_data = final_data[final_data["Label"]==1]
#extracting only the individual caregivers from the start time used in Webscrape and merge file to webscrape the data
final_data = final_data[final_data["CreatedTime"]>= "2019-07-19"]
print(final_data["Message"])
final_data["UniversalMessageId"] = final_data["UniversalMessageId"].astype(str)

#exproting the individual caregivers to an excel file
final_data.to_excel("C:/Users/rgosai/PycharmProjects/twitter bot/retweet_data.xlsx")

