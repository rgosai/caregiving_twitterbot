# Caregiving_twitterbot

A twitterbot to identify individual caregiver and replying an appropriate link.

Make sure to run the program in the below mentioned order only:
1. webscrape and merge.py
2. Clean_Data.py
3. ML_algorithms_1.py
4. web_scrapping.py

Note:
1. Please change the date manually given in the program to webscrape the twitter data for that week on line 30 in webscrape and merge.py file.

2. webscrape and merge.py code will take around one and half hours to run and webscrape data according to the filteres provided so please dont cancel
the code in the middle.

3. All the webscraped Sprinklr files are stored in Final data folder so please change the directory path accordingly while reading these file in webscrape 
and merge.py file

4. Substitute the "Created Time" in ML_algorithms_1.py file with the start date you have provided in the webscrape and merge.py


Steps that can be further implemented:

1. Broden the range of filters in order to extract the tweets according to the services provided by AARP for senior citizens.
2. Adding filters to extract finances and other data from Sprinklr by analyzing the keywords used in these kinds of tweets.