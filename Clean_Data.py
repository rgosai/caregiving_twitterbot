import pandas as pd
import nltk
import string
import re
#nltk.download()

def data_cleaning(data):

    data['Message'] = data['Message'].str.lower()
    #data['Message'] = data.Message.str.strip()
    #manually filtering the data with the given keywords
    data = data[data.Message.str.contains('mom | dad | parent | father | mother | mum | grandpa | grandma | grandparent | wife | husband', na=True, regex=True)]
    data = data[data['Message'].str.contains('dyslexia | dementia | dymentia | alzheimer | parkinson | caregiving | caregiver | care | caring | caretaking | caretaker', na=True, regex=True)]
    data = data.dropna(subset=['Message'])
    data['Message'] = data.Message.str.strip()
    data = data.drop_duplicates(subset=['Message'], keep='first')
    data = data.sort_values(by="CreatedTime", ascending=True)
    data["UniversalMessageId"] = data["UniversalMessageId"].astype(str)
    print(data)

    #removing unnecessary symbols, links, punctuations, duplicates and URLs
    data_1 = data[~data.Message.str.contains('http\S+|www.\S+|https\S+|www.\S+|lnkd\S+', na = True, regex=True)]
    data_1 = data_1[~data_1.Message.str.contains('ow.ly\S+|hubs.ly\S+|qoo.ly\S+|bddy\S+', na = True, regex=True)]
    data_1 = data_1[~data_1.Message.str.contains('bit.ly\S+|spr.ly\S+|buff.ly\S+|goo.gl\S+', na = True, regex=True)]
    data_1 = data_1.dropna(subset=['Message'])
    data_1['Message'] = data_1['Message'].str.replace('@[A-Za-z0-9_]+', "")
    data_1["Message"] = data_1["Message"].str.replace("rt ","")
    data_1['Message'] = data_1['Message'].str.replace('[A-Za-z]+.com\S+', "")
    data_1['Message'] = data_1['Message'].str.replace('[A-Za-z]+.edu\S+|[A-Za-z]+.org\S+', "")
    data_1['Message'] = data_1['Message'].str.replace('[A-Za-z0-9 ]+.com', "")
    data_1['Message'] = data_1['Message'].str.replace('#\w+', "")
    data_1['Message'] = data_1["Message"].replace('[^a-zA-Z0-9 ]', ' ', regex=True)
    data_1['Message'] = data_1['Message'].str.replace('\/\\\+=', " ")
    data_1['Message'] = data_1['Message'].str.replace('\s+', " ", regex=True)

    data_1['Message'] = data_1["Message"].apply(lambda x: ''.join([i if 32 < ord(i) < 126 else " " for i in x]))
    data_1['Message'] = data_1['Message'].apply(lambda x:''.join([i for i in x if i not in string.punctuation]))
    data_1['Message'] = data_1.Message.str.strip()
    data_1 = data_1.drop_duplicates(subset=['Message'], keep='first')
    # print(data)
    print(data_1)
    data_1["UniversalMessageId"] = data_1["UniversalMessageId"].astype(str)
    print(data_1["UniversalMessageId"].dtype.name)

    return (data, data_1)

if __name__ =="__main__":
    dat_1 = pd.read_excel("C:/Users/rgosai/PycharmProjects/twitter bot/combined_train_data.xlsx", encoding='utf-8')
    dat_2 = pd.read_excel("C:/Users/rgosai/PycharmProjects/twitter bot/combined_test_data.xlsx", encoding='utf-8')
    train_filtered_data, train_clean_data = data_cleaning(dat_1)
    test_filtered_data, test_clean_data = data_cleaning(dat_2)
    print(train_filtered_data)
    print(train_clean_data)
    print(test_filtered_data)
    print(test_clean_data)
    train_filtered_data.to_excel("C:/Users/rgosai/PycharmProjects/twitter bot/train_filtered_data.xlsx")
    train_clean_data.to_excel("C:/Users/rgosai/PycharmProjects/twitter bot/train_clean_data.xlsx")
    test_filtered_data.to_excel("C:/Users/rgosai/PycharmProjects/twitter bot/test_filtered_data.xlsx")
    test_clean_data.to_excel("C:/Users/rgosai/PycharmProjects/twitter bot/test_clean_data.xlsx")

    #test = data_cleaning(data_2)
    #test.to_csv("C:/Users/rgosai/PycharmProjects/twitter bot/unique_test_data.csv")
    # print(train.shape)
    # print(test.shape)
# data['Message'] = re.findall(r'\w+', data['Message'])
# freq = pd.Series(' '.join(data['Message']).split()).value_counts()[-10:]
#print(freq)

# print(data_1.shape)
# print(data_2)
#   print(data_2['Message'])
# data_1.to_csv("C:/Users/rgosai/PycharmProjects/twitter bot/unique_train_data.csv")
# data_2.to_csv("C:/Users/rgosai/PycharmProjects/twitter bot/unique_test_data.csv")







