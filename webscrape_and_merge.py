import pandas as pd
import tweepy
import random
import time
import csv
import os
import glob

#Twitter consumer and access keys
CONSUMER_KEY ='TLbNolNxtWUJsulKvocG9MrIC'
CONSUMER_SECRET ='5laDVcXIXSExiZhk1YJq2zKrvIhlWKLHxuyw2u0pW0G6xhrSqz'
ACCESS_TOKEN ='1152285699141439489-uVeeHyVC6extcz8cHtGXdF3rGVWJXk'
ACCESS_TOKEN_SECRET ='e6vyCo9cXVm054Kx0hf7AFIAgBeDLwynfwIDANIZQguhD'

auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
api = tweepy.API(auth, wait_on_rate_limit=True)

#defining keywords for searching tweets with those words
search_words = ["#caregiving", "#dementia", "#alzheimer", "#parkinson", "#dyslexia", "#caregiver", "#caretaker", "#caretaking"]

# csvFile = open('C:/Users/rgosai/PycharmProjects/twitter bot/result.csv', 'a', encoding="utf-8")

SenderScreenName, Message, Sender_Followers_Count, CreatedTime, UniversalMessageId = [],[],[],[], []

# csvWriter = csv.writer(csvFile)

#searching for tweets with the given search words and within the given time frame
for s in search_words:
    for tweet in tweepy.Cursor(api.search, q=s, since = "2019-07-19", until = "2019-07-25", lang="en").items():
        SenderScreenName.append(tweet.user.screen_name)
        Message.append(tweet.text.encode('utf-8'))
        Sender_Followers_Count.append(tweet.user.followers_count)
        CreatedTime.append(tweet.created_at)
        UniversalMessageId.append(str(tweet.id))

#converting to dataframe
zipped_list = list(zip(UniversalMessageId, SenderScreenName, Message, Sender_Followers_Count, CreatedTime))

df = pd.DataFrame(zipped_list, columns = ['UniversalMessageId', 'SenderScreenName', 'Message', 'Sender_Followers_Count', 'CreatedTime'])

print(df)

#indexing the dataframe
df.index = df.index.set_names(["Ind"])

#downloading the tweets to the excel sheet
df.to_excel("C:/Users/rgosai/PycharmProjects/twitter bot/combined_test_data.xlsx")

#to read data in other directory
os.chdir("C:/Users/rgosai/PycharmProjects/twitter bot/Final_health_data")

all_files = glob.glob("*.xlsx")
print(all_files)

li = []

#extracting the universal message id from the file and replacing unnecessary words and symbols appended to it
#and appending each file to list
for filename in all_files:
    df_1 = pd.read_excel(filename, ignore_index=False)
        #row.UniversalMessageId = row.UniversalMessageId.astype(str)
    df_1["UniversalMessageId"] = df_1["UniversalMessageId"].str.replace("TWITTER_[0-9]?_", "", regex=True)
    # print(df.dtypes)
    li.append(df_1)


#concatenating with empty dataframe
frame = pd.concat(li, axis=0, ignore_index=True, sort=True)
frame = frame.dropna(axis=1, how='all')

#extracting required columns
frame_1 = frame[["UniversalMessageId","SenderScreenName", "Message", "Sender Followers Count", "CreatedTime" , "Sentiment"]]
frame_1.columns = ["UniversalMessageId","SenderScreenName", "Message", "Sender_Followers_Count", "CreatedTime" , "Sentiment"]
print(frame_1.describe())



frame_1 = frame_1.drop("Sentiment", axis=1)

frame_1.index = frame_1.index.set_names(["Ind"])

print(frame_1)

data = pd.concat([df, frame_1], ignore_index=True)

print(data)

#downloading the data in an excel file
data.to_excel("C:/Users/rgosai/PycharmProjects/twitter bot/combined_train_data.xlsx")

